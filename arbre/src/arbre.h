#include <stdbool.h>

typedef struct maillon
{
	struct maillon *g;
	struct maillon *d;
	int v;
} Maillon, *Arbre;

typedef struct
{
	Arbre	c1;
	Arbre	c2;
}	Couple;

Arbre	arbreNouv(void);//
bool	vide(Arbre a);//
void	afficherRacine(int v, int k);//
Arbre	e(Arbre a, int x, Arbre b);//
void	afficherArbre(Arbre a);//
void	afficherArb(Arbre a, int k);//
Arbre	ag(Arbre a);//
Arbre	ad(Arbre a);//
int	r(Arbre a);//
int	max(int a, int b);//
int	nn(Arbre a);//
bool	f(Arbre a);//
int	nf(Arbre a);//
Arbre	eg(Arbre a);//
Arbre	ed(Arbre a);//
int	h(Arbre a);//
void	viderArbre(Arbre a);//
int	nn(Arbre a);//
bool	egarb(Arbre a, Arbre b);//
Arbre	insf(Arbre a, int x);//
Arbre	supp(Arbre a, int x);//
Arbre	oterMax(Arbre a);//
Arbre	rech(Arbre a, int x);//
bool	trie(Arbre a);//
Arbre	rd(Arbre a);//
Arbre	rg(Arbre a);//
Arbre	rgd(Arbre a);//
Arbre	rdg(Arbre a);//
Arbre	reeq(Arbre a);//
int	deseq(Arbre a);//
Arbre	insr(Arbre a, int x);//
Couple	couper(Arbre a, int x);//
Arbre	oterMin(Arbre a);//
