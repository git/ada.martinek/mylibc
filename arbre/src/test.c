#include "arbre.h"
#include <stdio.h>

void	testEg(bool a)
{
	if(a)
		printf("EgArb ok\n");
	else
		printf("EgArb nope\n");
	return;
}

void	testFeuille(bool a)
{
	if(a)
		printf("c'est une feuille\n");
	else
		printf("c'est un baobab\n");
	return;
}

void	testTri(bool a)
{
	if(a)
		printf("arbre trié\n");
	else
		printf("arbre non trié\n");
	return;
}

int	main(void)
{
	Arbre Arbre1 = arbreNouv();
	Arbre1 = insf(Arbre1, 11);
	Arbre1 = insf(Arbre1, 10);
	Arbre1 = insf(Arbre1, 9);
	Arbre1 = insf(Arbre1, 8);
	Arbre1 = insf(Arbre1, 7);
	Arbre1 = insf(Arbre1, 6);
	Arbre1 = insf(Arbre1, 5);
	afficherArbre(Arbre1);
	Arbre1 = supp(Arbre1, 10);
	afficherArbre(Arbre1);
	viderArbre(Arbre1);
	return 0;
}
