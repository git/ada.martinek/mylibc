#include <stdio.h>
#include <stdlib.h>
#include "arbre.h"

Arbre	arbreNouv(void) //
{
	return NULL;
}

void	viderArbre(Arbre a) //
{
	if(vide(a))
	{
		return;
	}
	viderArbre(a->g);
	viderArbre(a->d);
	free(a);
}

bool	vide(Arbre a) //
{
	return a == NULL;
}

void	afficherRacine(char *v, int k) //
{
	int	i;
	
	i = 0;
	while(i < k)
	{
		printf("---");
		i++;
	}
	printf(">%s\n", v);
}

Arbre	e(Arbre a, char *x, Arbre b) //
{
	Maillon *m;

	m = (Maillon *)malloc(sizeof(Maillon));
	if(!m)
	{
		printf("erreur allocation memoire enracinement");
		exit(1);
	}
	m->v = x;
	m->g = a;
	m->d = b;
	return m;
}

void	afficherArbre(Arbre a) //
{
	afficherArb(a, 0);
}

void	afficherArb(Arbre a, int k) //
{
	if(vide(a))
	{
		return;
	}
	afficherArb(ad(a), k + 1);
	afficherRacine(r(a), k);
	afficherArb(ag(a), k + 1);
}

Arbre	ag(Arbre a) //
{
	if(vide(a))
	{
		printf("operation interdite ag");
		exit(1);
	}
	return a->g;
}

Arbre	ad(Arbre a) //
{
	if(vide(a))
	{
		printf("operation interdite ad");
		exit(1);
	}
	return a->d;
}

char	*r(Arbre a) //
{
	if(vide(a))
	{
		printf("operation interdite racine");
		exit(1);
	}
	return a->v;
}

int	h(Arbre a) //
{
	if(vide(a))
	{
		return -1;
	}
	return 1 + (h(ag(a)) > h(ad(a)) ? h(ag(a)) : h(ad(a)));
}

bool	f(Arbre a) //
{
	if(vide(a))
	{
		return false;
	}
	return vide(ag(a)) && vide(ad(a));
}

int	nf(Arbre a) //
{
	if(vide(a))
	{
		return 0;
	}
	if(f(a))
	{
		return 1;
	}
	return nf(ag(a)) + nf(ad(a));
}

Arbre	eg(Arbre a) //
{
	if(vide(a))
	{
		return a;
	}
	if(vide(ag(a)))
	{
		return a;
	}
	return eg(ag(a));
}


Arbre	ed(Arbre a) //
{
	if(vide(a))
	{
		return a;
	}
	if(vide(ad(a)))
	{
		return a;
	}
	return ed(ad(a));
}

int	nn(Arbre a) //
{
	if(vide(a))
		return 0;
	return 1 + nn(ag(a)) + nn(ad(a));
}

int	max(char *a, char *b) //
{
	return my_strcmp(a, b);
}

bool	egarb(Arbre a, Arbre b) //
{
	if(vide(a) && vide(b))
		return true;
	if(vide (a) || vide(b))
		return false;
	return r(a) == r(b) && egarb(ag(a),ag(b)) && egarb(ad(a), ad(b));
}

Arbre	insf(Arbre a, char *x) //
{
	if(vide(a))
	{
		return e(arbreNouv(), x, arbreNouv());
	}
	if (my_strcmp(x, r(a)) == 0)
		return a;
	if (my_strcmp(x, r(a)) < 0)
	{
		a->g = insf(a->g, x);
		return reeq(a);
	}
	a->d = insf(a->d, x);
	return reeq(a);
}

Arbre	supp(Arbre a, char *x) //
{
	Arbre	tmp;
	
	if(vide(a))
		return a;
	if(my_strcmp(x, r(a)) == 0)
	{
		if(vide(ag(a)))
		{
			tmp = a->d;
			return tmp;
		}
		if(vide(ad(a)))
		{
			tmp = a->g;
			return tmp;
		}
		tmp = e(oterMax(a->g), r(ed(a->g)), a->d);
		return tmp;
	}
	if(my_strcmp(x, r(a)) > 0)
	{
		tmp = e(supp(a->g, x), r(a), a->d);
		return tmp;
	}
	tmp = e(a->g, r(a), supp(a->d, x));
	return tmp;
}

Arbre	oterMax(Arbre a) //
{
	Arbre	tmp;

	if(vide(a))
		return a;
	if(vide(ad(a)))
	{
		tmp = a->g;
		free(a);
		return tmp;
	}
	a->d = oterMax(a->d);
	return a;
}

Arbre	rech(Arbre a, char *x) //
{
	if(vide(a) || my_strcmp(r(a), x) == 0)
		return a;
	if(my_strcmp(x, r(a)) < 0)
		return rech(ag(a), x);
	return rech(ad(a), x);
}
bool	trie(Arbre a) //
{
	bool	b1;
	bool	b2;
	Arbre	tmp;

	b1 = false;
	b2 = false;
	if(vide(a))
		return true;
	if(vide(ag(a)))
		b1 = true;
	else
	{
		tmp = ed(ag(a));
		b1 = r(tmp) < r(a);
	}
	if(vide(ad(a)))
		b2 = true;
	else
	{
		tmp = eg(ad(a));
		b2 = r(tmp) > r(a);
	}
	return b1 && b2 && trie(ag(a)) && trie(ad(a));
}

Arbre	rd(Arbre a)
{
	Arbre	tmp;

	if(vide(a))
	{
		printf("arbre gauche vide pour retournement droit\n");
		exit(1);
	}
	tmp = e(ag(ag(a)), r(ag(a)),  e(ad(ag(a)), r(a), ad(a)));
	free(a);
	return tmp;
}

Arbre	rg(Arbre a)
{
	Arbre	tmp;

	if(vide(a))
	{
		printf("arbre droit vide pour retournement gauche\n");
		exit(1);
	}
	tmp = e(e(ag(a), r(a), ag(ad(a))), r(ad(a)), ad(ad(a)));
	free(a);
	return tmp;
}

Arbre	rgd(Arbre a)
{
	if(vide(a))
		return a;
	a->g = rg(a->g);
	return rd(a);
}

Arbre	rdg(Arbre a)
{
	if(vide(a))
		return a;
	a->d = rd(a->d);
	return rg(a);
}

Arbre	reeq(Arbre a)
{
	if(deseq(a) == 2 && 0 <= deseq(ag(a)))
		a = rd(a);
	if(deseq(a) == -2 && deseq(ad(a)) <= 0)
		a = rg(a);
	if(deseq(a) == 2 && -1 == deseq(ag(a)))
		a = rgd(a);
	if(deseq(a) == -2 && 1 == deseq(ad(a)))
		a = rdg(a);
	return a;
}

int	deseq(Arbre a)
{
	if(vide(a))
		return 0;
	return h(ag(a)) - h(ad(a));
}

Arbre	insr(Arbre a, char *x)
{
	Couple	c;

	c = couper(a, x);
	return reeq(e(c.c1, x, c.c2));
}
Couple	couper(Arbre a, char *x)
{
	Couple	c;
	Couple	d;

	if(vide(a))
	{
		c.c1 = arbreNouv();
		c.c2 = arbreNouv();
		return c;
	}
	if(my_strcmp(x, r(a)) == 0)
	{
		c.c1 = ag(a);
		c.c2 = ad(a);
		return c;
	}
	if(my_strcmp(x, r(a)) < 0)
	{
		d = couper(ag(a), x);
		c.c1 = d.c1;
		c.c2 = e(d.c2, r(a), ad(a));
		return c;
	}
	d = couper(ad(a), x);
	c.c1 = e(ag(a), r(a), d.c1);
	c.c2 = d.c2;
	return c;
}
Arbre	oterMin(Arbre a)
{
	Arbre	tmp;

	if(vide(a))
		return a;
	if(vide(ag(a)))
	{
		tmp = ad(a);
		free(a);
		return tmp;
	}
	a->g = oterMin(a->g);
	return a;
}

