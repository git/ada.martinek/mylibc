#include <stdbool.h>

#define MAX(X, Y) (((X) > (Y)) ? (X) : (Y))

typedef struct maillon
{
	struct maillon *g;
	struct maillon *d;
	char *v;
} Maillon, *Arbre;

typedef struct
{
	Arbre	c1;
	Arbre	c2;
}	Couple;

Arbre	arbreNouv(void);//
bool	vide(Arbre a);//
void	afficherRacine(char *v, int k);//
Arbre	e(Arbre a, char *x, Arbre b);//
void	afficherArbre(Arbre a);//
void	afficherArb(Arbre a, int k);//
Arbre	ag(Arbre a);//
Arbre	ad(Arbre a);//
char	*r(Arbre a);//
int	max(char *a, char *b);//
int	nn(Arbre a);//
bool	f(Arbre a);//
int	nf(Arbre a);//
Arbre	eg(Arbre a);//
Arbre	ed(Arbre a);//
int	h(Arbre a);//
void	viderArbre(Arbre a);//
int	nn(Arbre a);//
bool	egarb(Arbre a, Arbre b);//
Arbre	insf(Arbre a, char *x);//
Arbre	supp(Arbre a, char *x);//
Arbre	oterMax(Arbre a);//
Arbre	rech(Arbre a, char *x);//
bool	trie(Arbre a);//
Arbre	rd(Arbre a);//
Arbre	rg(Arbre a);//
Arbre	rgd(Arbre a);//
Arbre	rdg(Arbre a);//
Arbre	reeq(Arbre a);//
int	deseq(Arbre a);//
Arbre	insr(Arbre a, char *x);//
Couple	couper(Arbre a, char *x);//
Arbre	oterMin(Arbre a);//
int	my_strcmp(char *s1, char *s2);
int	my_strlen(char *str);
