#include "arbre.h"
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

void	catch_int(int sig_num)
{
	signal(SIGINT, catch_int);
	printf("lol ta cru\n signal %d\n", sig_num);
	//fflush(stdout);
}

void	saisie(char **mot)
{
	char	*buffer = NULL;
	size_t	size = 0;
	size_t	len = 0;
	char	c;

	printf("Saisissez votre mot\n");
	while((c = getchar()) != '\n' && c != EOF);
	getline(&buffer, &size, stdin);
	len = my_strlen(buffer);
	if(buffer[len - 1] == '\n')
	{
		buffer[len - 1] = '\0';
		len--;
	}
	*mot = realloc(buffer, len + 1);
	if(*mot == NULL)
	{
		free(buffer);
		printf("Erreur de réallocation de mémoire\n");
		exit(1);
	}
	return;
}

int	main(void)
{
	char	h[10];
	Arbre	Arbre1 = arbreNouv();
	char	*mot = NULL;

	signal(SIGINT, catch_int);
	while(1)
	{
		printf("\nString B-tree\n1 - Afficher\n2 - Ajouter\n3 - Rechercher\n4 - Supprimer\nAciaobonsoirmespetitsloupsaumiel - Quitter\n");
		scanf("%s", h);
		if(my_strcmp(h, "2") == 0)
		{
			saisie(&mot);
			Arbre1 = insf(Arbre1, mot);
			printf("\n");
			afficherArbre(Arbre1);
		}
		else if(my_strcmp(h, "1") == 0)
		{
			afficherArbre(Arbre1);
		}
		else if(my_strcmp(h, "3") == 0)
		{
			saisie(&mot);
			afficherArbre(rech(Arbre1, mot));
		}
		else if(my_strcmp(h, "4") == 0)
		{
			saisie(&mot);
			Arbre1 = supp(Arbre1, mot);
			afficherArbre(Arbre1);

		}
		else if(my_strcmp(h, "Aciaobonsoirmespetitsloupsaumiel") == 0)
		{
			break;
		}
		else
		{
			printf("choix invalide\n");
		}
	}
	return 0;
}
