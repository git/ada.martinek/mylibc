int	is_prime(int nb)
{
	unsigned int	i;

	i = 2;
	while (i <= nb / i)
	{
		if (nb % i == 0)
			return (1);
		i++;
	}
	return (0);
}

int	find_next_prime(int nb)
{
	if (nb <= 2)
		return (2);
	while (!(is_prime(nb) == 0))
		nb++;
	return (nb);
}
