#include "graphe.h"

Graphe	graphNouv()
{
	return NULL;
}

Graphe	adjst(Graphe g, int x)
{
	MaillonG *m;

	m = (MaillonG*)malloc(sizeof(MaillonG));
	if(m == NULL)
	{
		printf("probleme malloc MaillonG\n");
		exit(1);
	}
	m->v = x;
	m->suiv = g;
	m->l = listenouv();
	return m;
}

Graphe	adjs(Graphe g, int x)
{
	if(videGraphe(g))
		return adjst(g, x);
	if(x < g->v)
		return adjst(g, x);
	if(x == g->v)
		return g;
	g->suiv = adjs(g->suiv, x);
	return g;
}

Graphe	adja(Graphe g, int x, int y)
{
	Graphe m;

	m = g;
	if(!exs(g, x) || !exs(g, y))
		return g;
	while(g->v != x)
		g = g->suiv;
	g->l = insList(g->l, y);
	return m;
}

Graphe	sups1(Graphe g, int x)
{
	Graphe m;

	if(g->v == x)
	{
		m = g;
		g = g->suiv;
		free(m);
		return g;
	}
	g->suiv = sups1(g->suiv, x);
	return g;
}

Graphe	sups(Graphe g, int x)
{
	if(!exs(g, x) || de(g, x) != 0 || di(g, x) != 0)
		return g;
	return sups1(g, x);
}

Graphe	supa(Graphe g, int x, int y)
{
	Graphe m;

	m = g;
	if(!exa(g, x, y))
		return g;
	while(g->v != x)
		g = g->suiv;
	g->l = supList(g->l, y);
	return m;
}

bool	exs(Graphe g, int s)
{
	if(videGraphe(g))
		return false;
	if(s < g->v)
		return false;
	if(s == g->v)
		return true;
	return exs(g->suiv, s);
}


bool	exa(Graphe g, int x, int y)
{
	if(!exs(g, x) || !exs(g, y))
		return false;
	while(g->v != x)
		g = g->suiv;
	return rechList(g->l, y);
}


int	de(Graphe g, int x)
{
	while(!videGraphe(g))
	{
		if(g->v == x)
			return lgList(g->l);
		g = g->suiv;
	}
	return 0;
}

int	di(Graphe g, int x)
{
	int cpt;

	cpt = 0;
	while(!videGraphe(g))
	{
		if(rechList(g->l, x))
			cpt++;
		g = g->suiv;
	}
	return cpt;
}

Liste	esg(Graphe g/*, int x*/)
{
	Liste res = listenouv();
	if(videGraphe(g))
		return res;
	while(g != NULL)
	{
		res = insList(res, g->v);
		g = g->suiv;
	}
	return res;
}

bool	videGraphe(Graphe g)
{
	return g == NULL;
}

Liste	esuc(Graphe g, int x)
{
	if(!exs(g, x))
		return listenouv();
	while(g->v != x)
		g = g->suiv;
	return g->l;
}

Liste	parcoursProfondeur(Graphe g, Liste e, Liste l)
{
	int	k;
	Liste	l1;

	if(videList(e))
		return l;
	k = tList(e);
	if(rechList(l, k))
		l1 = l;
	else
		l1 = parcoursProfondeur(g, copie(esuc(g, k)), insList(l, k));
	return parcoursProfondeur(g, supList(e, k), l1);
}

void	afficherGraphe(Graphe g)
{
	FILE	*flot;
	Graphe	m;
	Liste	aux;

	m = g;
	flot = fopen("graphe.txt", "w");
	if(flot == NULL)
	{
		printf("probleme ouverture fichier affichage graphe\n");
		exit(1);
	}
	fprintf(flot, "digraph family\n{\n");
	while(!videGraphe(g))
	{
		fprintf(flot, "%d[label=\"S%d\"]\n", g->v, g->v);
		g = g->suiv;
	}
	while(!videGraphe(m))
	{
		aux = m->l;
		while(aux != NULL)
		{
			fprintf(flot, "%d->%d\n", m->v, aux->v);
			aux = aux->suiv;
		}
		m = m->suiv;
	}
	fprintf(flot, "}\n");
	fclose(flot);
	system("dotty graphe.txt");
}

Graphe	supts(Graphe g)
{
	Graphe	aux = g;

	g = g->suiv;
	free(aux);
	return g;
}
