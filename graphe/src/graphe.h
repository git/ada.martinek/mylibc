#include <stdbool.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdio.h>
#include "liste.h"

typedef struct maillonG
{
	int	v;
	struct	maillonG *suiv;
	Liste	l;
} MaillonG, *Graphe;

Graphe	graphNouv();
Graphe	adjst(Graphe g, int x);
Graphe	adjs(Graphe g, int x);
Graphe	adja(Graphe g, int x, int y);
Graphe	sups1(Graphe g, int x);
Graphe	sups(Graphe g, int x);
Graphe	supa(Graphe g, int x, int y);
bool	exs(Graphe g, int s);
bool	exa(Graphe g, int x, int y);
int	de(Graphe g, int x);
int	di(Graphe g, int x);
Liste	esg(Graphe g/*, int x*/);
bool	videGraphe(Graphe g);
Liste	esuc(Graphe g, int x);
Liste	parcoursProfondeur(Graphe g, Liste e, Liste l);
void	afficherGraphe(Graphe g);
Graphe	supts(Graphe g);
