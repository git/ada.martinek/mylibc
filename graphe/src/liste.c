#include "liste.h"

Liste	listenouv(void)
{
	return NULL;
}

bool	videList(Liste l)
{
	return l == NULL;
}


Liste	insTList(Liste l, int x)
{
	Maillon*	maill;

	maill = (Maillon*)malloc(sizeof(Maillon));
	if(maill == NULL)
	{
		printf("Probleme malloc Maillon Liste\n");
		exit(1);
	}
	maill->v = x;
	maill->suiv = l;
	return maill;
}

Liste	insList(Liste l, int x)
{
	if(videList(l))
		return insTList(l, x);
	if(x < l->v)
		return insTList(l,x);
	if(x == l->v)
		return l;
	l->suiv = insList(l->suiv, x);
	return l;
}

Liste	supTList(Liste l)
{
	Maillon*	aux;
	
	if(videList(l))
	{
		printf("Opération interdite suppression tete liste\n");
		exit(1);
	}
	aux = l;
	l = l->suiv;
	free(aux);
	return l;
}

Liste	supList(Liste l, int x)
{
	if(videList(l))
		return l;
	if(x < l->v)
		return l;
	if(x == l->v)
		return supTList(l);
	l->suiv = supList(l->suiv, x);
	return l;
}

int	tList(Liste l)
{
	if(videList(l))
	{
		printf("Opération interdite tListe\n");
		exit(1);
	}
	return l->v;

}

bool	rechList(Liste l, int x)
{
	if(videList(l))
		return false;
	if(x < l->v)
		return false;
	if(x == l->v)
		return true;
	return rechList(l->suiv, x);
}

int	lgList(Liste l)
{
	if(videList(l))
		return 0;
	return 1 + lgList(l->suiv);
}

Liste	addQList(Liste l, int x)
{
	if(videList(l))
		return insTList(l,x);
	l->suiv = addQList(l->suiv, x);
	return l;
}

void	afficherList(Liste l)
{
	if(videList(l))
	{
		printf("\n");
		return;
	}
	printf("%d\n", l->v);
	afficherList(l->suiv);
}

Liste	cope(Liste l)
{
	Liste c = NULL;

	while(l != NULL)
	{
		c = insList(c, l->v);
		l = l->suiv;
	}
	return c;
}