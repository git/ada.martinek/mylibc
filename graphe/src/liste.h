#include <stdbool.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdio.h>

typedef struct maillon
{
	int	v;
	struct	maillon *suiv;
} Maillon, *Liste;

Liste	listenouv	(void);
bool	videList	(Liste l);
Liste	insTList	(Liste l, int x);
Liste	insList		(Liste l, int x);
Liste	supTList	(Liste l);
Liste	supList		(Liste l, int x);
int	tList		(Liste l);
bool	rechList	(Liste l, int x);
int	lgList		(Liste l);
Liste	addQList	(Liste l, int x);
void 	afficherList	(Liste l);
Liste	copie(Liste l);