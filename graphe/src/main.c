#include "graphe.h"

void	testList()
{
	Liste l = listenouv();

	l = insList(l, 3);
	l = insList(l, 5);
	l = insList(l, 1);
	l = insList(l, 9);
	l = insList(l, 20);
	l = insList(l, 7);
	afficherList(l);
	l = supList(l, 1);
	l = supList(l, 4);
	l = supList(l, 9);
	afficherList(l);
	printf("Valeur en tete %d => 3\n", tList(l));
	if(rechList(l, 1))
		printf("Rech marche pas\n");
	else
		printf("Rech marche\n");
	if(rechList(l, 20))
		printf("Rech marche\n");
	else
		printf("Rech marche pas\n");
	printf("lgList %d => 4\n", lgList(l));
}

void	testGraphe()
{
	Graphe g = graphNouv();

	g = adjs(g, 3);
	g = adjs(g, 6);
	g = adjs(g, 3);
	g = adjs(g, 9);
	g = adjs(g, 8);
	g = adjs(g, 15);
	g = adjs(g, 23);
	if(exs(g, 3))
		printf("exs marche\n");
	else
		printf("exs marche pas \n");
	if(exs(g, 415))
		printf("exs marche pas\n");
	else
		printf("exs marche\n");
	g = adja(g, 3, 6);
	g = adja(g, 6, 15);
	g = adja(g, 9, 8);
	g = adja(g, 9, 6);
	g = adja(g, 9, 15);
	g = adja(g, 23, 15);
	if(exa(g, 3, 5))
		printf("exa marche pas\n");
	else
		printf("exa marche\n");
	if(exa(g, 3, 6))
		printf("exa marche\n");
	else
	 	printf("exa marche pas\n");
	afficherGraphe(g);
	g = supa(g, 9, 6);
	afficherGraphe(g);
	g = sups(g, 15);
	g = supa(g, 3, 6);
	g = sups(g, 3);
	afficherGraphe(g);
	afficherList(esg(g));
}

int	main(void)
{
	//testList();
	testGraphe();
	return 0;
}
