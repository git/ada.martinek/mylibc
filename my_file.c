#include <stdio.h>
#include <stdbool.h>
#define TYPE int

typedef struct maillon
{
	TYPE		v;
	struct maillon	*suiv;
} Maillon, *File;

File	filenouv(void)
{
	return NULL;
}

bool	vide(File f)
{
	return f==NULL;
}

TYPE	tete(File f)
{
	if(vide(f))
	{
		printf("Op�ration interdite\n");
		exit (1);
	}
	return f->suiv->v;
}

int	longueur(File f)
{
	if(vide(f))
	{
		printf("Op�ration interdite\n");
		exit (1);
	}
	Maillon	*m;
	int	i;
	i = 1;
	m = f->suiv;
	while(m != f)
	{
	++i
	m = m->suiv;
	}
	return i;
}

/*void	afficher(File f)
{
	if(vide(f))
	{
		printf("File vide\n");
		return;
	Maillon	*m;
	m = f->suiv;
	while(m != f)
	{
		printf("%d\t", m->v);
		m = m->suiv;
	}
	printf("\n");
}*/

File	supprTete(File f)
{
	if(vide(f))
	{
		printf("Op�ration interdite\n");
		exit (1);
	}
	if(f == f->suiv)
	{
		free(f);
		return NULL;
	}
	Maillon	*tmp;
	tmp = f->suiv;
	tmp->suiv = tmp->suiv->suiv;
	free(f);
	return tmp;
}

File	addQueue(File f, TYPE x)
{
	Maillon	*m;
	if(!m = (Maillon *)malloc(sizeof(Maillon)))
	{
		printf("Probl�me Malloc\n");
		exit (1);
	}
	m->v = x;
	if(vide(f))
	{
		f = m;
		m->suiv = m;
		return f;
	}
	m->suiv = f->suiv;
	f->suiv = m;
	f = m;
	return f;
}
