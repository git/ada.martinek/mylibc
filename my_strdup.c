#include <stdlib.h>

char	*my_strdup(char *src)
{
	int	size;
	char	*new;
	int	i;

	size = 0;
	i = 0;
	while (src[size])
		size++;
	new = (char*)malloc(sizeof(char) * (size + 1));
	if (!new)
		return (NULL);
	while (src[i])
	{
		new[i] = src[i];
		i++;
	}
	new[i] = '\0';
	return (new);
}
