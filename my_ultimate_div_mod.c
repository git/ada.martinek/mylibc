void	ultimate_div_mod(int *a, int *b)
{
	int buf_a;
	int buf_b;

	buf_a = *a;
	buf_b = *b;
	*a = buf_a / buf_b;
	*b = buf_a % buf_b;
}
